# echoes list of rules defined in the pipeline

echo "rules in cluster.yml :"
echo $(cat cluster.yml | grep -v "^ " | grep ":$" | grep -v "^#" | grep -v "^__default" | cut -f1 -d ":" | sort)

echo "rules in config.yml :"
echo $(cat config.yml | grep -v "^ " | grep ":$" | cut -f1 -d ":" | sort)

echo "all rules :"
echo $(cat rules/* | grep "rule " | cut -f1 -d ":" | cut -f2 -d " " | sort)
