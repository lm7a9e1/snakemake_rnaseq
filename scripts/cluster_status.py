#!/usr/bin/env python
import subprocess
import sys

jobid = sys.argv[1]

output = subprocess.check_output("qstat -fx %s | grep Exit_status | cut -d '=' -f2 | tr -d ' '" % jobid, shell=True).decode()
if output == "":
    #No exit status, job is hold, queued or running
    print("running")
    exit(0)

exit_code = int(output)
if exit_code < 0 or exit_code > 0:
    #Job killed by PBS if output <0
    #Job execution failed if output >0
    print("failed")
else:
    print("success")
