### Usage
# python filter_bam.py bam id_list sam
#     bam: bam input file
#     id_list
#     sam: sam output file
#
# Isn't made to be used directly, the script is called be the "clean_sam"
# rule defined in rules/bam_processing.smk

import pysam
import argparse

parser = argparse.ArgumentParser(description="Filter out bam file dfor offending reads detected with Picard ValidateSam")

parser.add_argument("input_bam", type=str)
parser.add_argument("ids", type=str)
parser.add_argument("output_sam", type=str)

args = parser.parse_args()

with open(args.ids) as f:
    qnames = f.read().splitlines

bam = pysam.AlignmentFile(args.input_bam)
osam = pysam.AlignmentFile(args.output_sam, "w", template=bam)

for b in bam.fetch(until_eof=True):
    if b.query_name not in qnames:
            osam.write(b)
 
bam.close()
osam.close()

