# Uses the multiqc API to extract the max adapter content from a list of fastqc files

# Usage
# python multiqc_adapter_content.py directory -t threshold
#      directory: location of the fastqc files
#      threshold: adapter presence tolerance, default 0.01
#
# Isn't made to be used directly, the script is called be the "adapter_content"
# rule defined in rules/general_stats.smk

from multiqc.utils import report
from multiqc.utils import config

import argparse

parser = argparse.ArgumentParser(description="Read the adapter presence in a multiqc report")

parser.add_argument("analysis_dir", type=str)
parser.add_argument("-t", "--threshold", 
                    dest="threshold", 
                    default=0.01, 
                    type=float,
                    help="Adapter presence tolenrance thresold")

args = parser.parse_args()

config.analysis_dir = [config.analysis_dir[0] + "/" +  args.analysis_dir]
config.data_dir = None

report.get_filelist(['fastqc'])

fastqc_module = config.avail_modules['fastqc'].load()

try:
    fastqc_module = fastqc_module()
except UserWarning:
    raise ValueError("Could not find valid fastqc output in: {}".format(config.analysis_dir[0]))

fastqc_data = fastqc_module.fastqc_data

adapter_max_name = "no adapter"
adapter_max_value = 0

for filename in fastqc_data:
    content = fastqc_data[filename]['adapter_content']
    for pos in content:
        for adapter in [k for k in pos if k != "position"]:
            if pos[adapter] > adapter_max_value:
                adapter_max_name = adapter
                adapter_max_value = pos[adapter]

print("{}: {}".format(adapter_max_name, adapter_max_value))
if adapter_max_value >= args.threshold :
    raise UserWarning("presence of adapters")
