#!/usr/bin/env bash
. /appli/bioinfo/snakemake/5.4.0/env.sh

### Set paths
workdir="$SCRATCH"

script_path="$PWD/scripts"

sample_info="$PWD/sample_info/sample_info_juvenile.csv"
rawdata_path="/home/ref-bioinfo/ifremer/rmpf/Juvenile_couleur_rnaseq/data/rna-sequence-raw"

### Dry-run pipeline and save dag
snakemake $@ --configfile config.yml \
             --config workdir_path=$workdir script_path=$script_path sample_info=$sample_info rawdata_path=$rawdata_path\
             --forceall \
             --dag | dot -Tpdf > dag.pdf

. /appli/bioinfo/snakemake/5.4.0/delenv.sh
