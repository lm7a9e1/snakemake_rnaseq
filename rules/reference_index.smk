def reference_fasta_input(wildcards):
    reference = config['reference'][wildcards.ref + '_fasta']
    return reference

def reference_gff3_input(wildcards):
    return config['reference'][wildcards.ref + '_gff3']

#gsnap

rule gmap_index:
    input:
        reference_fasta_input
    output:
        directory("02_reference_index/gmap/{ref}")
    params:
        out_dir="02_reference_index/gmap",
        conda_env=config['gmap_index']['conda_env'],
        sorting_order=config['gmap_index']['sort']
    log:
        "logs/02_reference_index/gmap/{ref}.log"
    benchmark:
        "benchmarks/02_reference_index-gmap-{ref}.txt"
    shell:
        "set +u ;"
        "source activate {params.conda_env} ;"
        "gmap_build --dir={params.out_dir} {input} -d {wildcards.ref} -s {params.sorting_order} >& {log} 2>&1 ;"
        "conda deactivate ; set -u"

#bwa

ruleorder: bwa_index_copy > bwa_index

rule bwa_index:
    input:
        reference=reference_fasta_input
    output:
        bwt="02_reference_index/bwa/{ref}/{fasta_name}.bwt",
        pac="02_reference_index/bwa/{ref}/{fasta_name}.pac",
        ann="02_reference_index/bwa/{ref}/{fasta_name}.ann",
        amb="02_reference_index/bwa/{ref}/{fasta_name}.amb",
        sa= "02_reference_index/bwa/{ref}/{fasta_name}.sa",
        fa= "02_reference_index/bwa/{ref}/{fasta_name}"
    log:
        "logs/02_reference_index/bwa/bwa_index_{fasta_name}_ref{ref}.log"
    benchmark:
        "benchmarks/02_reference_index-bwa-{fasta_name}_ref{ref}.txt"
    shell:
       "set +u ;"
        ". /appli/bioinfo/bwa/0.7.15/env.sh ;"
        "ln -rs {input.reference} {output.fa} ;"
        "bwa index {output.fa} >& {log} 2>&1 ;"
        "conda deactivate ; set -u"

def bwa_indexed_reference_input(wildcards):
    fasta = reference_fasta_input(wildcards)
    return {"fa"  : fasta,
            "bwt" : fasta + ".bwt",
            "pac" : fasta + ".pac",
            "ann" : fasta + ".ann",
            "amb" : fasta + ".amb",
            "sa"  : fasta + ".sa"}

rule bwa_index_copy:
    input:
        unpack(bwa_indexed_reference_input)
    output:
        bwt="02_reference_index/bwa/{ref}/{fasta_name}.bwt",
        pac="02_reference_index/bwa/{ref}/{fasta_name}.pac",
        ann="02_reference_index/bwa/{ref}/{fasta_name}.ann",
        amb="02_reference_index/bwa/{ref}/{fasta_name}.amb",
        sa= "02_reference_index/bwa/{ref}/{fasta_name}.sa",
        fa= "02_reference_index/bwa/{ref}/{fasta_name}"
    log:
        "logs/02_reference_index/bwa/bwa_index_copy_{fasta_name}_ref{ref}.log"
    benchmark:
        "benchmarks/02_reference_index-bwa_index_copy-{fasta_name}_ref{ref}.txt"
    shell:
       "set +u ;"
        "ln -rs {input.bwt} {output.bwt} ;"
        "ln -rs {input.pac} {output.pac} ;"
        "ln -rs {input.ann} {output.ann} ;"
        "ln -rs {input.amb} {output.amb} ;"
        "ln -rs {input.sa} {output.sa} ;"
        "ln -rs {input.fa} {output.fa} ;"
        "set -u"

#STAR

rule star_index:
    input:
       reference=reference_fasta_input,
       gff3=reference_gff3_input
    output:
       "02_reference_index/star/{ref}/chrLength.txt",
       "02_reference_index/star/{ref}/chrName.txt",
       "02_reference_index/star/{ref}/exonGeTrInfo.tab",
       "02_reference_index/star/{ref}/geneInfo.tab",
       "02_reference_index/star/{ref}/genomeParameters.txt",
       "02_reference_index/star/{ref}/SAindex",
       "02_reference_index/star/{ref}/sjdbList.fromGTF.out.tab",
       "02_reference_index/star/{ref}/transcriptInfo.tab",
       "02_reference_index/star/{ref}/chrNameLength.txt", 
       "02_reference_index/star/{ref}/chrStart.txt", 
       "02_reference_index/star/{ref}/exonInfo.tab", 
       "02_reference_index/star/{ref}/Genome",     
       "02_reference_index/star/{ref}/SA",    
       "02_reference_index/star/{ref}/sjdbInfo.txt",  
       "02_reference_index/star/{ref}/sjdbList.out.tab"
    params:
       ncpus=config['star_index']['ncpus'],
       overhang=config['star_index']['overhang'],
       gChrBinNbits=config['star_index']['gChrBinNbits'],
       mem=config['star_index']['mem'],
       tagTranscript=config['star_index']['tagTranscript'],
       tagGene=config['star_index']['tagGene']
    log:
       "logs/02_reference_index/star/star_index_ref{ref}.log"
    benchmark:
       "benchmarks/02_reference_index-star-ref{ref}.txt"
    shell:
       "set +u ;"
       ". /appli/bioinfo/star/2.7.0d/env.sh ;"
       "mkdir -p 02_reference_index/star/{wildcards.ref} ;"
       "STAR --runMode genomeGenerate "
       "--runThreadN {params.ncpus} "
       "--genomeDir 02_reference_index/star/{wildcards.ref} "
       "--genomeFastaFiles {input.reference} "
       "--sjdbOverhang {params.overhang} "
       "--genomeChrBinNbits {params.gChrBinNbits} "
       "--limitGenomeGenerateRAM {params.mem} "
       "--sjdbGTFfile {input.gff3} " 
       "--sjdbGTFtagExonParentTranscript {params.tagTranscript} "
       "sjdbGTFtagExonParentGene {params.tagGene} "
       ">& {log} 2>&1 ;"
       "conda deactivate ; set -u"
