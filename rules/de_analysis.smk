def sartools_targetfile_input(wildcards):
    sample_info = config["sample_info"]
    return sample_info
    
rule sartools_targetfile:
    input:
        sartools_targetfile_input
    output:
        "06_de_analysis/targetfile{process}.tsv"
    params:
        conda_env = config['sartools']['conda_env'],
        script_path = config['script_path'],
        sample_info = config["sample_info"],
        var_int = config['sartools']['var_int'],
        batch = config['sartools']['batch']
    log:
        "logs/06_de_analysis/targetfile{process}.log"
    shell:
        "set +u ;"
        "source activate {params.conda_env} ;"
        "Rscript {params.script_path}/create_sartools_targetfile.R"
        "    {params.sample_info}"
        "    {output}"
        "    {params.var_int}"
        "    {wildcards.process}"
        "    {params.batch}"
        "    >& {log} 2>&1 ;"
        "conda deactivate ; set -u"

def sartools_input(wildcards):
    mqc_report = "99_general_stats/tag_counting#ref{ref}_{mapping}{process}_multiqc_report.html",
    htseq_files = expand("05_tag_counting/{name}{process}.bam_htseq-count.txt",
            name = SAMPLE_NAMES, process = wildcards.process)
    return {"reoprt" : mqc_report, "files" : htseq_files}

rule sartools:
    input:
        unpack(sartools_input),
        target_file = "06_de_analysis/targetfile{process}.tsv"
    output:
        report(directory("06_de_analysis/sartools{process}"))
    params:
        conda_env = config['sartools']['conda_env'],
        script_path = config['script_path'],
        sample_info = config["sample_info"],
        raw_dir = "../../05_tag_counting",
        project_name = config['sartools']['project_name'],
        author = config['sartools']['author'],
        features_to_remove = config['sartools']['features_to_remove'],
        var_int = config['sartools']['var_int'],
        cond_ref = config['sartools']['cond_ref'],
        batch = config['sartools']['batch'],
        fit_type = config['sartools']['fit_type'],
        cooks_cutoff = config['sartools']['cooks_cutoff'],
        independent_filtering = config['sartools']['independent_filtering'],
        alpha = config['sartools']['alpha'],
        p_adjust_method = config['sartools']['p_adjust_method'],
        type_trans = config['sartools']['type_trans'],
        loc_func = config['sartools']['loc_func'],
        colors = config['sartools']['colors'],
    log:
        "logs/06_de_analysis/sartools{process}.log"
    benchmark:
        "benchmarks/06_de_analysis-sartools{process}.txt"
    shell:
        "set +u ;"
        "source activate {params.conda_env} ;"
        "mkdir -p {output} ;"
        "Rscript {params.script_path}/Sartools.R"
        "    --targetFile ../../{input.target_file}"
        "    --workDir {output}"
        "    --rawDir {params.raw_dir}"
        "    --projectName \"{params.project_name}\""
        "    --author \"{params.author}\""
        "    --featuresToRemove {params.features_to_remove}"
        "    --varInt \"{params.var_int}\""
        "    --condRef \"{params.cond_ref}\""
        "    --batch \"{params.batch}\""
        "    --fitType {params.fit_type}"
        "    --cooksCutoff {params.cooks_cutoff}"
        "    --independentFiltering {params.independent_filtering}"
        "    --alpha {params.alpha}"
        "    --pAdjustMethod {params.p_adjust_method}"
        "    --typeTrans {params.type_trans}"
        "    --locfunc {params.loc_func}" 
        "    --colors {params.colors}"
        "    >& {log} 2>&1 ;"
        "conda deactivate ; set -u"
