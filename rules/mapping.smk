def reference_gff3_input(wildcards):
    return config['reference'][wildcards.ref + '_gff3']

def reference_fasta_input(wildcards):
    reference = config['reference'][wildcards.ref + '_fasta']
    return reference

def reference_fasta_name_input(wildcards):
    import os
    reference = config['reference'][wildcards.ref + '_fasta']
    reference = os.path.basename(reference)
    return reference

#gsnap

def reference_gsnap_path_input(wildcards):
    return config['reference'][wildcards.ref + '_gsnap_path']

def reference_gsnap_name_input(wildcards):
    reference = config['reference'][wildcards.ref + '_gsnap_name']
    return reference

def reference_gsnap_path_name_input(wildcards):
    return reference_gsnap_path_input(wildcards) + "/" + reference_gsnap_name_input(wildcards)

rule gsnap:
    input:
        reference=reference_gsnap_path_name_input,
        r1="01_trimming/{name}_R1_paired.fastq.gz",
        r2="01_trimming/{name}_R2_paired.fastq.gz"
    output:
        "03_mapping/gsnap/{name}_ref{ref}.halfmapping_transloc",
        "03_mapping/gsnap/{name}_ref{ref}.paired_uniq_inv",
        "03_mapping/gsnap/{name}_ref{ref}.paired_uniq_long",
        "03_mapping/gsnap/{name}_ref{ref}.unpaired_transloc",
        "03_mapping/gsnap/{name}_ref{ref}.paired_uniq_scr",
        "03_mapping/gsnap/{name}_ref{ref}.concordant_transloc",
        "03_mapping/gsnap/{name}_ref{ref}.unpaired_uniq",
        "03_mapping/gsnap/{name}_ref{ref}.halfmapping_uniq",
        "03_mapping/gsnap/{name}_ref{ref}.halfmapping_mult",
        "03_mapping/gsnap/{name}_ref{ref}.unpaired_mult",
        "03_mapping/gsnap/{name}_ref{ref}.nomapping",
        "03_mapping/gsnap/{name}_ref{ref}.concordant_mult",
        "03_mapping/gsnap/{name}_ref{ref}.concordant_uniq"
    params:
        conda_env=config['gsnap']['conda_env'],
        ncpus=config['gsnap']['ncpus'],

        reference_path=reference_gsnap_path_input,
        reference_name=reference_gsnap_name_input,

        min_coverage=config['gsnap']['min_coverage'],
        max_mismatch=config['gsnap']['max_mismatch'],
        nvl_splicing=config['gsnap']['nvl_splicing'],
        platform=config['gsnap']['platform'],
        out_format=config['gsnap']['out_format'],
        split_output="03_mapping/gsnap/{name}_ref{ref}",
    log:
        "logs/03_mapping/gsnap/{name}_ref{ref}.log"
    benchmark:
        "benchmarks/03_mapping-gsnap-{name}_ref{ref}.txt"
    shell:
        "set +u ;"
        "source activate {params.conda_env} ;"
        "gsnap --gunzip -t {params.ncpus} -A {params.out_format}" 
        "    --min-coverage={params.min_coverage}"
        "    --dir={params.reference_path}"
        "    -d {params.reference_name}"
        "    --max-mismatches={params.max_mismatch}"
        "    --novelsplicing={params.nvl_splicing}"
        "    --split-output={params.split_output}"
        "    --read-group-id={wildcards.name}_ref{wildcards.ref}"
        "    --read-group-platform={params.platform}"
        "    {input.r1} {input.r2} >& {log} 2>&1 ;"
        "conda deactivate ; set -u"


rule samtools_view:
    input:
        "03_mapping/gsnap/{name}.{type}"
    output:
        temp("03_mapping/gsnap_samtools_view/{name}.{type}.bam")
    #wildcard_constraints:
    #    type="concortand_uniq|paired_uniq_inv|paired_uniq_long"
    log:
        "logs/03_mapping/gsnap_samtools_view/{name}.{type}.log"
    benchmark:
        "benchmarks/03_mapping-gsnap_samtools_view-{name}.{type}.txt"
    shell:
        "set +u ;"
        ". /appli/bioinfo/samtools/1.9/env.sh ;"
        "samtools view"
        "    -b {input} >& {output}"
        "    2> {log} ;"
        "conda deactivate ; set -u"

rule samtools_merge:
    input:
        concordant_uniq="03_mapping/gsnap_samtools_view/{name}.concordant_uniq.bam",
        paired_uniq_inv="03_mapping/gsnap_samtools_view/{name}.paired_uniq_inv.bam",
        paired_uniq_long="03_mapping/gsnap_samtools_view/{name}.paired_uniq_long.bam"
    output:
        bam="03_mapping/bam/{name}_gsnap.bam"
    params:
    log:
        "logs/03_mapping/bam/{name}_gsnap.log"
    benchmark:
        "benchmarks/03_mapping-bam-{name}_gnsap.txt"
    shell:
        "set +u ;"
        ". /appli/bioinfo/samtools/1.9/env.sh ;"
        "samtools merge"
        "    -f {output}" 
        "       {input.concordant_uniq}"
        "       {input.paired_uniq_inv}"
        "       {input.paired_uniq_long}"
        "     2> {log} ;"
        "conda deactivate ; set -u"

#bwa

def bwa_local_indexed_reference_input(wildcards):
    basename = reference_fasta_name_input(wildcards)
    return {"fa"  : "02_reference_index/bwa/" + wildcards.ref + "/" + basename,
            "bwt" : "02_reference_index/bwa/" + wildcards.ref + "/" + basename + ".bwt",
            "pac" : "02_reference_index/bwa/" + wildcards.ref + "/" + basename + ".pac",
            "ann" : "02_reference_index/bwa/" + wildcards.ref + "/" + basename + ".ann",
            "amb" : "02_reference_index/bwa/" + wildcards.ref + "/" + basename + ".amb",
            "sa"  : "02_reference_index/bwa/" + wildcards.ref + "/" + basename + ".sa"}

rule bwa:
    input:
        unpack(bwa_local_indexed_reference_input),
        r1="01_trimming/{name}_R1_paired.fastq.gz",
        r2="01_trimming/{name}_R2_paired.fastq.gz"
    output:
        bam="03_mapping/bam/{name}_ref{ref}_bwa.bam"
    params:
        ncpus=config['bwa']['ncpus'],
        platform=config['bwa']['platform']
    log:
        "logs/03_mapping/bam/{name}_ref{ref}_bwa.log"
    benchmark:
        "benchmarks/03_mapping-bwa-{name}_ref{ref}.txt"
    shell:
       "set +u ;"
        ". /appli/bioinfo/bwa/0.7.15/env.sh ;"
        "bwa mem -t {params.ncpus} "
        " -R \"@RG\tID:{wildcards.name}_ref{wildcards.ref}\tLB:{wildcards.name}_ref{wildcards.ref}\tPL:{params.platform}\tPU:{wildcards.name}_ref{wildcards.ref}\tSM:{wildcards.name}_ref{wildcards.ref}\""
        " -M {input.fa} "
        " {input.r1} {input.r2} "
        " >& {output.bam} 2> {log} ;"
        "conda deactivate ; set -u"

#STAR

def reference_star_index_input(wildcards):
    return ["02_reference_index/star/" + wildcards.ref + "/chrLength.txt",
    "02_reference_index/star/" + wildcards.ref + "/chrName.txt",
    "02_reference_index/star/" + wildcards.ref + "/exonGeTrInfo.tab",
    "02_reference_index/star/" + wildcards.ref + "/geneInfo.tab",
    "02_reference_index/star/" + wildcards.ref + "/genomeParameters.txt",
    "02_reference_index/star/" + wildcards.ref + "/SAindex",
    "02_reference_index/star/" + wildcards.ref + "/sjdbList.fromGTF.out.tab",
    "02_reference_index/star/" + wildcards.ref + "/transcriptInfo.tab",
    "02_reference_index/star/" + wildcards.ref + "/chrNameLength.txt", 
    "02_reference_index/star/" + wildcards.ref + "/chrStart.txt", 
    "02_reference_index/star/" + wildcards.ref + "/exonInfo.tab", 
    "02_reference_index/star/" + wildcards.ref + "/Genome",     
    "02_reference_index/star/" + wildcards.ref + "/SA",    
    "02_reference_index/star/" + wildcards.ref + "/sjdbInfo.txt",  
    "02_reference_index/star/" + wildcards.ref + "/sjdbList.out.tab"]            

rule star:
    input:
        reference_star_index_input,
        r1="01_trimming/{name}_R1_paired.fastq.gz",
        r2="01_trimming/{name}_R2_paired.fastq.gz"
    output:
        bam="03_mapping/bam/{name}_ref{ref}_star.bam",
    params:
        output_dir="03_mapping/star",
        prefix="03_mapping/star/{name}_ref{ref}.",
        tmp_dir="03_mapping/star/{name}_ref{ref}_star_tmpdir",
        ncpus=config['star']['ncpus'],
        readFilesCommand=config['star']['readFilesCommand'],
        outFilterMultimapNmax=config['star']['outFilterMultimapNmax'],
        alignSJoverhangMin=config['star']['alignSJoverhangMin'],
        alignSJDBoverhangMin=config['star']['alignSJDBoverhangMin'],
        outFilterMismatchNmax=config['star']['outFilterMismatchNmax'],
        outFilterMismatchNoverLmax=config['star']['outFilterMismatchNover'],
        alignIntronMin=config['star']['alignIntronMin'],
        alignIntronMax=config['star']['alignIntronMax'],
        alignMatesGapMax=config['star']['alignMatesGapMax'],
        outSAMmapqUnique=config['star']['outSAMmapqUnique'],
        outSAMtype=config['star']['outSAMtype']
    log:
        "logs/03_mapping/star/{name}_ref{ref}_star.log"
    benchmark:
        "benchmarks/03_mapping-star-{name}_ref{ref}.txt"
    shell:
        "set +u ;"
        "mkdir -p {params.output_dir} ;"
        " rm -rf {params.tmp_dir} ;"
        ". /appli/bioinfo/star/2.7.0d/env.sh ;"
        "STAR --runThreadN {params.ncpus} "
        "--genomeDir 02_reference_index/star/{wildcards.ref} "
        "--readFilesIn {input.r1} {input.r2} "
        "--outFileNamePrefix {params.prefix} "
        "--readFilesCommand {params.readFilesCommand} "
        "--outFilterMultimapNmax {params.outFilterMultimapNmax} "
        "--alignSJoverhangMin {params.alignSJoverhangMin} "
        "--alignSJDBoverhangMin {params.alignSJDBoverhangMin} "
        "--outFilterMismatchNmax {params.outFilterMismatchNmax} "
        "--outFilterMismatchNoverLmax {params.outFilterMismatchNoverLmax} "
        "--alignIntronMin {params.alignIntronMin} "
        "--alignIntronMax {params.alignIntronMax}"
        "--alignMatesGapMax {params.alignMatesGapMax} "
        "--outSAMmapqUnique {params.outSAMmapqUnique} "
        "--outSAMtype {params.outSAMtype} "
        "--outSAMattrRGline ID:{wildcards.name}_ref{wildcards.ref} LB:{wildcards.name}_ref{wildcards.ref} PL:{wildcards.name}_ref{wildcards.ref} "
        "PU:{wildcards.name}_ref{wildcards.ref} SM:{wildcards.name}_ref{wildcards.ref} "
        "--outTmpDir {params.tmp_dir} "
        ">& {log} 2>&1 ;"
        "mv {params.prefix}Aligned.out.bam {output.bam} ;" 
        "conda deactivate ; set -u"

