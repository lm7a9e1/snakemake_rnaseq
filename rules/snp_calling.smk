def reference_gff3_input(wildcards):
    return config['reference'][wildcards.ref + '_gff3']

def reference_fasta_input(wildcards):
    reference = config['reference'][wildcards.ref + '_fasta']
    return reference

def reference_fasta_name_input(wildcards):
    import os
    reference = config['reference'][wildcards.ref + '_fasta']
    reference = os.path.basename(reference)
    return reference

def freebayes_bamlist_input(wildcards):
    bam_files = expand("04_bam_processing/{sample}_ref{ref}_{mapping}{process}.bam",
           sample = SAMPLE_NAMES, 
           ref = wildcards.ref, 
           mapping = wildcards.mapping, 
           process = wildcards.process)
    sorted_bam_files = expand("04_bam_processing/{sample}_ref{ref}_{mapping}{process}_sorted.bam",
           sample = SAMPLE_NAMES,
           ref = wildcards.ref,
           mapping = wildcards.mapping,
           process = wildcards.process)
    bai_files = expand("04_bam_processing/{sample}_ref{ref}_{mapping}{process}_sorted.bam.bai",
            sample = SAMPLE_NAMES,
            ref = wildcards.ref,
            mapping = wildcards.mapping,
            process = wildcards.process)
    return {"bam" : bam_files, "sorted" : sorted_bam_files, "bai" : bai_files}

rule freebayes_bamlist:
    input:
        unpack(freebayes_bamlist_input)
    output:
        #temp("07_snp_calling/bamlist_{mapping}{process}_sorted.tsv")
        "07_snp_calling/bamlist_ref{ref}_{mapping}{process}_sorted.tsv"
    log:
        "logs/07_snp_calling/bamlist-ref{ref}_{mapping}{process}_sorted.log"
    shell:
        "set +u ;"
        "ls {input.sorted} > {output} ;"
        "set -u"

rule freebayes:
    input:
        unpack(freebayes_bamlist_input),
        reference = reference_fasta_input,
        bamlist = "07_snp_calling/bamlist_ref{ref}_{mapping}{process}_sorted.tsv"
    output:
    params:
        nalleles = config['freebayes']['use_best_n_alleles'],
        minmapq = config['freebayes']['min_mapping_quality'],
        mincov = config['freebayes']['min_coverage'],
        add_params = config['freebayes']['add_params']
    log:
        logs="logs/07_snp_calling/snpcalling_freebayes_ref{ref}_{mapping}{process}_sorted.log",   
        vcf="07_snp_calling/snpcalling_freebayes_ref{ref}_{mapping}{process}_sorted.vcf"
    benchmark:
        "benchmarks/07_snp_calling-freebayes_ref{ref}_{mapping}{process}_sorted.txt"
    shell:
        "set +u ;"
         ". /appli/bioinfo/freebayes/1.3.1/env.sh ;"
         "freebayes -f {input.reference}"
         " --use-best-n-alleles {params.nalleles}"
         #" --debug"
         " --min-mapping-quality {params.minmapq}"
         " --min-coverage {params.mincov}"
         " --bam-list {input.bamlist}"
         #" --vcf {output}"
         " {params.add_params}"
         " --vcf {log.vcf}" #debug
         " >& {log.logs} 2>&1 ;"
         #" >& {output} 2>&1 ;" #debug
         "conda deactivate ; set -u"


def mpileup_input(wildcards):
     bam_files = expand("04_bam_processing/{sample}_ref{ref}_{mapping}{process}.bam",
            sample = SAMPLE_NAMES,
            ref = wildcards.ref, 
            mapping = wildcards.mapping,
            process = wildcards.process)
     return {"bam" : bam_files}


rule mpileup_RGlist:
    output:
        "07_snp_calling/mpileup_RGlist_ref{ref}_{mapping}{process}_sorted.tsv"
    params:
        sample_names = lambda w:expand("{name}_ref{ref}", name = SAMPLE_NAMES, ref = w.ref)
    log:
        "logs/07_snp_calling/mpileup_RGlist_ref{ref}_{mapping}{process}_sorted.log"
    shell:
        "set +u ;"
        "echo \"{params.sample_names}\" | tr ' ' '\\n' > {output} ;"
        "set -u"

rule mpileup:
    input:
        unpack(mpileup_input),
        reference = reference_fasta_input,
        RGlist = "07_snp_calling/mpileup_RGlist_ref{ref}_{mapping}{process}_sorted.tsv"
    output:
        "07_snp_calling/snpcalling_mpileup_ref{ref}_{mapping}{process}_sorted.vcf"
    params:
    log:
        "logs/07_snp_calling/snpcalling_mpileup_ref{ref}_{mapping}{process}_sorted.log"
    benchmark:
        "benchmarks/07_snp_calling-mpileup_ref{ref}_{mapping}{process}_sorted.txt"
    shell:
         "set +u ;"
         ". /appli/bioinfo/bcftools/1.4.1/env.sh ;"
         #"bcftools mpileup -Ou --ignore-RG -f {input.reference} {input.bam} | bcftools call -mv -Ov -o {output}"
         "bcftools mpileup -Ou --read-groups {input.RGlist} -f {input.reference} {input.bam} | bcftools call -mv -Ov -o {output}"
         " >& {log} 2>&1 ;"
         "conda deactivate ; set -u"

rule mpileup_ignore_RG:
    input:
        unpack(mpileup_input),
        reference = reference_fasta_input,
    output:
        "07_snp_calling/snpcalling_mpileup_ref{ref}_{mapping}{process}_ignore_RG_sorted.vcf"
    params:
    log:
        "logs/07_snp_calling/snpcalling_mpileup_ref{ref}_{mapping}{process}_ignore_RG_sorted_.log"
    benchmark:
        "benchmarks/07_snp_calling-mpileup_ref{ref}_{mapping}{process}_ignore_RG_sorted.txt"
    shell:
        "set +u ;"
         ". /appli/bioinfo/bcftools/1.4.1/env.sh ;"
         "bcftools mpileup -Ou --ignore-RG -f {input.reference} {input.bam} | bcftools call -mv -Ov -o {output}"
         " >& {log} 2>&1 ;"
         "conda deactivate ; set -u"

rule vcftools:
    input:
        "07_snp_calling/snpcalling_{snpcalling}_ref{ref}_{mapping}{process}_sorted.vcf"
    output:
        vcf_filtered="07_snp_calling/snpcalling_{snpcalling,freebayes|mpileup}_ref{ref}_{mapping}{process}_sorted_filtered.vcf"
    params:
        tmp_dir=config['vcftools']['tmp_dir'],
        max_missing=config['vcftools']['max_missing'],
        maf=config['vcftools']['maf'],
        minDP=config['vcftools']['minDP'],
        add_params=config['vcftools']['add_params']
    log:
        "logs/07_snp_calling/filter_vcftools_{snpcalling}_ref{ref}_{mapping}{process}_sorted_filtered.log"
    benchmark:
        "benchmarks/07_snp_calling-filter_vcftools_{snpcalling}_ref{ref}_{mapping}{process}_sorted_filtered.txt"
    shell:
        "set +u ;"
        ". /appli/bioinfo/vcftools/0.1.16/env.sh ; "
        "vcftools --vcf {input} "
        " --out {params.tmp_dir}/tmp "
        " --recode "
        " --max-missing {params.max_missing} "
        " --maf {params.maf} "
        " --minDP {params.minDP} "
        " {params.add_params}"
        " >& {log} 2>&1 ;"
        " mv {params.tmp_dir}/tmp.recode.vcf {output.vcf_filtered} ;"
        "conda deactivate ; set -u"
