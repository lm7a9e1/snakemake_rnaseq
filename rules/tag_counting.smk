def reference_gff3_input(wildcards):
    return config['reference'][wildcards.ref + '_gff3']

rule htseq_count:
    input:
        gff3 = reference_gff3_input,
        bam = "04_bam_processing/{sample}_ref{ref}_{mapping}{process}_sorted.bam",
        bai = "04_bam_processing/{sample}_ref{ref}_{mapping}{process}_sorted.bam.bai"
    output:
        "05_tag_counting/{sample}_ref{ref}_{mapping}{process}_sorted.bam_htseq-count.txt"
    log:
        "logs/05_tag_counting/{sample}_ref{ref}_{mapping}{process}.log"
    benchmark:
        "benchmarks/_tag_counting-{sample}_ref{ref}_{mapping}{process}.txt"
    params:
        f = config['htseq_count']['f'],
        s = config['htseq_count']['s'],
        r = config['htseq_count']['r'],
        t = config['htseq_count']['t'],
        i = config['htseq_count']['i']
    shell:
        "set +u ;"
        ". /appli/bioinfo/htseq-count/0.9.1/env.sh ;"
        "htseq-count -f {params.f} -s {params.s} -r {params.r} -t {params.t} -i {params.i}"
        "    {input.bam} {input.gff3} >& {output} 2> {log};"
        "conda deactivate ; set -u"
