wildcard_constraints:
    state="\w+"

def fastqc_raw_input(wildcards):
    return expand("{path}/{name}.fastq.gz", path=DATAPATH, name=wildcards.name)

rule fastqc_raw:
    input: 
        file=fastqc_raw_input
    output:
        "99_general_stats/fastqc_raw/{name}_fastqc.zip",
        "99_general_stats/fastqc_raw/{name}_fastqc.html"
    log:
        "logs/99_general_stats/fastqc_raw/{name}.log"
    benchmark:
        "benchmarks/99_general_stats-fastqc_raw-{name}.txt"
    params:
        out_dir="99_general_stats/fastqc_raw"
    shell:
        "set +u ; "
        ". /appli/bioinfo/fastqc/0.11.8/env.sh ;"
        "fastqc {input.file} -o {params.out_dir} >& {log} 2>&1 ;"
        "conda deactivate ; set -u"

rule fastqc_trimmed:
    input: 
        file="01_trimming/{name}_paired.fastq.gz"
    output:
        "99_general_stats/fastqc_trimmed/{name}_paired_fastqc.zip",
        "99_general_stats/fastqc_trimmed/{name}_paired_fastqc.html"
    log:
        "logs/99_general_stats/fastqc_trimmed/{name}.log"
    benchmark:
        "benchmarks/99_general_stats-fastqc_trimmed-{name}.txt"
    params:
        out_dir="99_general_stats/fastqc_trimmed"
    shell:
        "set +u ; "
        ". /appli/bioinfo/fastqc/0.11.8/env.sh ;"
        "fastqc {input.file} -o {params.out_dir} >& {log} 2>&1 ;"
        "conda deactivate ; set -u"

rule samtools_flagstat:
    input:
        "04_bam_processing/{name}.bam"
    output:
        "99_general_stats/flagstat/{name}.bam.flagstat"
    log:
        "logs/99_general_stats/flagstat/{name}.log"
    benchmark:
        "benchmarks/99_general_stats_samtools_flagstat-{name}.txt"
    shell:
        "set +u ;"
        ". /appli/bioinfo/samtools/1.9/env.sh ;"
        "samtools flagstat {input} >& {output} 2> {log} ;"
        "conda deactivate ; set -u"

def multiqc_files_input(wildcards):
    if wildcards.state == "fastqc_raw":
        return expand("99_general_stats/{state}/{name}_{read}_fastqc{ext}", 
                      state = wildcards.state, 
                      name = SAMPLE_NAMES, 
                      read = ["R1", "R2"], 
                      ext = [".zip", ".html"])
    elif wildcards.state == "fastqc_trimmed":
        return expand("99_general_stats/{state}/{name}_{read}_paired_fastqc{ext}", 
                      state = wildcards.state, 
                      name = SAMPLE_NAMES, 
                      read = ["R1", "R2"], 
                      ext = [".zip", ".html"])
    elif wildcards.state == "flagstat":
        if wildcards.parameters :
            return expand("99_general_stats/flagstat/{name}_{parameters}{ext}", 
                          state = wildcards.state, 
                          name = SAMPLE_NAMES, 
                          parameters = wildcards.parameters,
                          ext = [".bam.flagstat"])
        else :
            return expand("99_general_stats/flagstat/{name}{ext}", 
                          state = wildcards.state, 
                          name = SAMPLE_NAMES, 
                          ext = [".bam.flagstat"])
    elif wildcards.state == "tag_counting":
        if wildcards.parameters :
            return expand("05_tag_counting/{name}_{parameters}{ext}", 
                          state = wildcards.state, 
                          name = SAMPLE_NAMES, 
                          parameters = wildcards.parameters,
                          ext = [".bam_htseq-count.txt"])
        else :
            return expand("05_tag_counting/{name}{ext}", 
                          state = wildcards.state, 
                          name = SAMPLE_NAMES, 
                          parameters = wildcards.parameters,
                          ext = [".bam_htseq-count.txt"])
    else:
        return []

rule adapter_content:
    input:
        multiqc_files_input
    log:
        adapter_content="99_general_stats/{state}/adapter_content.txt",
        warning="logs/99_general_stats/{state}/adapter_content.log"
    params:
        directory="99_general_stats/{state}",
        threshold=config['adapter_content']['threshold'],
        script_path=config['script_path']
    shell:
        "set +u ; "
        ". /appli/bioinfo/multiqc/1.5/env.sh ;"
        "python {params.script_path}/multiqc_adapter_content.py {params.directory}"
        "    -t {params.threshold} >& {log.adapter_content} 2> {log.warning} ;"
        "conda deactivate ; set -u"

rule multiqc:
    input:
        multiqc_files_input
    output:
        report="99_general_stats/{state}_multiqc_report.html",
        data_dir=directory("99_general_stats/{state}_multiqc_report_data")
    log:
        "logs/multiqc/{state}.log"
    shell:
        "set +u ; "
        ". /appli/bioinfo/multiqc/1.5/env.sh ;"
        "multiqc {input} -f -n {output.report} >& {log} 2>&1 ;"
        "conda deactivate ; set -u"

rule multiqc_with_params:
    input:
        multiqc_files_input
    output:
        report="99_general_stats/{state}#{parameters}_multiqc_report.html",
        data_dir=directory("99_general_stats/{state}#{parameters}_multiqc_report_data")
    log:
        "logs/multiqc/{state}_{parameters}.log"
    shell:
        "set +u ; "
        ". /appli/bioinfo/multiqc/1.5/env.sh ;"
        "multiqc {input} -f -n {output.report} >& {log} 2>&1 ;"
        "conda deactivate ; set -u"

