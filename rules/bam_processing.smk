wildcard_constraints:
    f_param="\d+",
    F_param="\d+",
    q_param="\d+"

rule process_bam:
    input:
        "99_general_stats/fastqc_trimmed/adapter_content.txt",
        "99_general_stats/fastqc_trimmed_multiqc_report.html",
        "03_mapping/bam/{name}.bam"
    output:
        #temp("04_bam_processing/{name}.bam")
        "04_bam_processing/{name}.bam"
    shell:
        "ln -sr {input} {output}"

ruleorder : process_bam > samtools_sort_by_position
#ruleorder : samtools_mark_duplicates > process_bam #utile ?

rule samtools_sort_by_position:
    input:
        "04_bam_processing/{name}.bam"
    output:
        sort="04_bam_processing/{name}_sorted.bam"
    log:
        sort="logs/04_bam_processing/{name}_sort.log"
    benchmark:
        "benchmarks/04_bam_processing-sort-{name}.txt"
    shell:
        "set +u ;"
        ". /appli/bioinfo/samtools/1.9/env.sh ;"
        "samtools sort {input} >& {output.sort} 2> {log.sort} ;"
        "conda deactivate ; set -u"

rule samtools_index:
    input:
        "04_bam_processing/{name}_sorted.bam"
    output:
        index="04_bam_processing/{name}_sorted.bam.bai"
    log:
       index="logs/04_bam_processing/{name}_index.log"
    benchmark:
       "benchmarks/04_bam_processing-index-{name}.txt"
    shell:
        "set +u ;"
        ". /appli/bioinfo/samtools/1.9/env.sh ;"
        "samtools index {input} >& {output.index} 2> {log.index} ;"
        "conda deactivate ; set -u"

rule filter_bam_fqF:
    input:
        "04_bam_processing/{name}.bam"
    output:
        filtered="04_bam_processing/{name}_f{f_param}_q{q_param}_F{F_param}.bam",
    log:
        filtered="logs/04_bam_processing/{name}_f{f_param}_q{q_param}_F{F_param}.log",
    benchmark:
        "benchmarks/04_bam_processing-{name}_f{f_param}_q{q_param}_F{F_param}.txt",
    params:
    shell:
        "set +u ;"
        ". /appli/bioinfo/samtools/1.9/env.sh ;"
        "samtools view -b -f{wildcards.f_param}"
        "                 -q {wildcards.q_param}"
        "                 -F {wildcards.F_param}"
        "                 {input} >& {output.filtered} 2> {log.filtered} ;"
        "conda deactivate ; set -u"


rule validate_sam:
    input:
        #"99_general_stats/flagstat#ref{ref}_{mapping}_multiqc_report.html",
        bam = "04_bam_processing/{sample}_ref{ref}_{mapping}.bam"
    output:
        validation_report = temp("04_bam_processing/{sample}_ref{ref}_{mapping}_validateSam.report"),
        id_list = "04_bam_processing/{sample}_ref{ref}_{mapping}_validateSam.list"
    params:
        picardtools = config['validate_sam']['picard_tools'],
        tmpdir = config['validate_sam']['tmp_dir'],
        validation_stringency = config['validate_sam']['validation_stringency']
    log:
        validation = "logs/04_bam_processing/{sample}_ref{ref}_{mapping}_validateSam_validation.log",
        cut = "logs/04_bam_processing/{sample}_ref{ref}_{mapping}_validateSam_cut.log"
    benchmark:
        "benchmarks/04_bam_processing-validate_same-{sample}_ref{ref}_{mapping}_validateSam.txt"
    shell:
        "set +u ;"
        "module load java/1.8.0_121 ; "
        "java -jar -Djava.io.tmpdir={params.tmpdir}"
        " {params.picardtools}/ValidateSamFile.jar"
        " I={input.bam}"
        " O={output.validation_report}"
        " VALIDATION_STRINGENCY={params.validation_stringency}"
        " >& {log.validation} 2>&1 || true ; "
        " cut -f6 -d \" \" {output.validation_report} | cut -f1 -d\",\" >& {output.id_list} 2> {log.cut} ;"
        "set -u"

rule clean_sam:
    input:
        #"99_general_stats/flagstat#ref{ref}_{mapping}_multiqc_report.html",
        bam = "04_bam_processing/{sample}_ref{ref}_{mapping}.bam",
        id_list = "04_bam_processing/{sample}_ref{ref}_{mapping}_validateSam.list"
    output:
        sam = temp("04_bam_processing/{sample}_ref{ref}_{mapping}_clean.sam"),
        bam = "04_bam_processing/{sample}_ref{ref}_{mapping}_clean.bam"
    params:
        pysam_env=config['clean_sam']['pysam_env'],
        script_path=config['script_path']
    log:
        python = "logs/04_bam_processing/{sample}_ref{ref}_{mapping}_clean_python.log",
        samtools = "logs/04_bam_processing/{sample}_ref{ref}_{mapping}_clean_samtools.log"
    benchmark:
        "benchmarks/04_bam_processing-clean_sam-{sample}_ref{ref}_{mapping}_clean.txt"
    shell:
        "set +u ;"
        "if [ ! -s {input.id_list} ] ;"
        "then source activate {params.pysam_env} ;"
        " python {params.script_path}/filter_bam.py"
        " {input.bam} {input.id_list} {output.sam}"
        "  >& {log.python} 2>&1 ;"
        " conda deactivate ;"
        " . /appli/bioinfo/samtools/1.9/env.sh ;"
        " samtools view -bS -o {output.bam} {output.sam}"
        "  >& {log.samtools} 2>&1 ; conda deactivate ;"
        " else touch {output.sam} ;"
        " cp {input.bam} {output.bam} ;"
        " fi ; "
        " set -u"


rule add_or_replace_read_groups:
    input:
        #"99_general_stats/flagstat#ref{ref}_{mapping}{process}_multiqc_report.html",
        bam = "04_bam_processing/{sample}_ref{ref}_{mapping}{process}_sorted.bam"
    output:
        "04_bam_processing/{sample}_ref{ref}_{mapping}{process}_sorted_RG.bam"
    params:
        picardtools = config['add_or_replace_read_groups']['picard_tools'],
        rgpl = config['add_or_replace_read_groups']['rgpl'],
        tmpdir = config['add_or_replace_read_groups']['tmp_dir'],
        validation = config['add_or_replace_read_groups']['validation_stringency']
    log:
        "logs/04_bam_processing/{sample}_ref{ref}_{mapping}{process}_sorted_RG.log"
    benchmark:
        "benchmarks/04_bam_processing-add_or_replace_read_groups-{sample}_ref{ref}_{mapping}{process}_sorted_RG.txt"
    shell:
        "set +u ;"
        "module load java/1.8.0_121 ; "
        "java -jar -Djava.io.tmpdir={params.tmpdir}"
        " {params.picardtools}/AddOrReplaceReadGroups.jar"
        " I={input.bam}"
        " O={output}"
        " RGID=\"{wildcards.sample}\""
        " RGLB=\"{wildcards.sample}\""
        " RGPL=\"{params.rgpl}\""
        " RGPU=\"{wildcards.sample}\""
        " RGSM=\"{wildcards.sample}\""
        " VALIDATION_STRINGENCY=\"{params.validation}\""
        " >& {log} 2>&1 ;"
        "set -u"

rule samtools_fixmate:
    input:
        #"99_general_stats/flagstat#ref{ref}_{mapping}{process}_multiqc_report.html",
        bam = "04_bam_processing/{sample}_ref{ref}_{mapping}{process}.bam"
    output:
        bam_sortn = temp("04_bam_processing/{sample}_ref{ref}_{mapping}{process}_sortn.bam"),
        bam_fixmate = temp("04_bam_processing/{sample}_ref{ref}_{mapping}{process}_sortn_fixmate.bam"),
        bam_sortp = "04_bam_processing/{sample}_ref{ref}_{mapping}{process}_fixmate.bam"
    log:
        "logs/04_bam_processing/{sample}_ref{ref}_{mapping}{process}_fixmate.log"
    benchmark:
        "benchmarks/04_bam_processing-fixmate-{sample}_ref{ref}_{mapping}{process}_fixmate.txt"
    shell:
         "set +u ;"
         ". /appli/bioinfo/samtools/1.9/env.sh ;"
         "samtools sort -n {input} >& {output.bam_sortn} 2> {log} ;"
         "samtools fixmate -m {output.bam_sortn} {output.bam_fixmate} >& {log} 2>&1 ;"
         "samtools sort {output.bam_fixmate} >& {output.bam_sortp} 2> {log} ;"
         "conda deactivate ; set -u"

rule samtools_mark_duplicates:
    input:
        #"99_general_stats/flagstat#ref{ref}_{mapping}{process}_multiqc_report.html",
        bam = "04_bam_processing/{sample}_ref{ref}_{mapping}{process}.bam"
    output:
        "04_bam_processing/{sample}_ref{ref}_{mapping}{process}_markdup.bam"
    log:
        "logs/04_bam_processing/{sample}_ref{ref}_{mapping}{process}_markdup.log"
    benchmark:
        "benchmarks/04_bam_processing-mark_duplicates-{sample}_ref{ref}_{mapping}{process}_markdup.txt"
    shell:
        "set +u ;"
        ". /appli/bioinfo/samtools/1.9/env.sh ;"
        "samtools markdup {input} {output} >& {log} 2>&1 ;"
        "conda deactivate ; set -u"

