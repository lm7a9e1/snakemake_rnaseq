def trim_input(wildcards):
    return {
        'r1':"{path}/{name}_R1.fastq.gz".format(path=DATAPATH, name=wildcards.name),
        'r2':"{path}/{name}_R2.fastq.gz".format(path=DATAPATH, name=wildcards.name)
    }

rule trim:
    input:
        unpack(trim_input)
    output:
        r1_single="01_trimming/{name}_R1_single.fastq.gz",
        r2_single="01_trimming/{name}_R2_single.fastq.gz",
        r1_paired="01_trimming/{name}_R1_paired.fastq.gz",
        r2_paired="01_trimming/{name}_R2_paired.fastq.gz"
    params:
        ncpus=config['trim']['ncpus'],
        adapters_file=config['trim']['adapters_file'],
        illuminaclip=config['trim']['illuminaclip'],
        leading=config['trim']['leading'],
        trailing=config['trim']['trailing'],
        slidingwindow=config['trim']['slidingwindow'],
        minlen=config['trim']['minlen']
    log:
        "logs/01_trimming/{name}.log"
    benchmark:
        "benchmarks/01_trimming-{name}.txt"
    shell:
        "set +u ; "
        ". /appli/bioinfo/trimmomatic/0.36/env.sh ;"
        "trimmomatic PE"
        "    -threads {params.ncpus}"
        "    -phred33"
        "    {input.r1}"
        "    {input.r2}"
        "    {output.r1_paired}"
        "    {output.r1_single}"
        "    {output.r2_paired}"
        "    {output.r2_single}"
        "    ILLUMINACLIP:{params.adapters_file}:{params.illuminaclip}"
        "    {params.leading}"
        "    {params.trailing}"
        "    {params.slidingwindow}"
        "    {params.minlen} "
        "    >& {log} 2>&1 ;" 
        "conda deactivate ; set -u"
