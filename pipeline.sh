#!/usr/bin/env bash
. /appli/bioinfo/snakemake/5.4.0/env.sh

### Set paths
workdir="$SCRATCH/rnasnake_juvenile"

script_path="$PWD/scripts"

sample_info="$PWD/sample_info/sample_info_juvenile.csv"
rawdata_path="/home/ref-bioinfo/ifremer/rmpf/Juvenile_couleur_rnaseq/data/rna-sequence-raw"

### Create log directory
mkdir -p $workdir/logs/snakemake_logs

### Log the run
target=$(grep -A 999 "input:" Snakefile | grep -v input | grep -v "^ *#")
config=$(cat config.yml)
cluster=$(cat cluster.yml)
joblist=$(snakemake $@ --configfile config.yml --config workdir_path=$workdir script_path=$script_path sample_info=$sample_info rawdata_path=$rawdata_path -nq)
timestamp=$(date -Iseconds | cut -f 1-3 -d - | cut -f 1 -d +)
run_log_filename="run_logs/run"
mkdir -p run_logs
run_log_filename="${run_log_filename}_${timestamp}.log"

echo "command: sh pipeline $@" >> ${run_log_filename}
echo "date: ${timestamp}" >> ${run_log_filename}
echo "sample_info: ${sample_info}" >> ${run_log_filename}
echo "rawdata_path: ${rawdata_path}" >> ${run_log_filename}
echo "tagret: ${target}" >> ${run_log_filename}
echo "${joblist}" >> ${run_log_filename}
echo "rules configuration:" >> ${run_log_filename}
echo "${config}\n\n" >> ${run_log_filename}
echo "cluster configuration:" >> ${run_log_filename}
echo "${cluster}\n\n" >> ${run_log_filename}

### Run the pipeline
snakemake $@ --stats stats.json \
             --keep-going \
             --rerun-incomplete \
             --verbose \
             --cluster-config cluster.yml \
             --configfile config.yml \
             --config workdir_path=$workdir script_path=$script_path sample_info=$sample_info rawdata_path=$rawdata_path\
             -p \
             --jobs 60 \
             --cluster "qsub -S '/bin/bash' -N {rule} -o '$workdir/logs/snakemake_logs' -e '$workdir/logs/snakemake_logs' -m n -q {cluster.queue} -l ncpus={cluster.ncpus} -l walltime={cluster.walltime} -l mem={cluster.memory}" \
             --cluster-status $script_path/cluster_status.py \
             --latency-wait 420

. /appli/bioinfo/snakemake/5.4.0/delenv.sh
