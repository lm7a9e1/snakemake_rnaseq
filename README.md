## A [snakemake](https://snakemake.readthedocs.io/en/stable/project_info/citations.html)  pipeline to perform RNA-seq data analysis.
The pipeline takes raw fastq files from paired-end RNA sequencing and processes them to differential expression analysis through six main steps.


### The pipeline goes through the following steps :
| Step | Name | What it going on there |
| ---- | ---- | -------------|
| 01 | Trimming |Remove sequencing adapters and discard low quality reads with [Trimmomatic](http://www.usadellab.org/cms/?page=trimmomatic) v0.36 (02_01) and perform post trimming quality control with [fastqc](https://www.bioinformatics.babraham.ac.uk/projects/fastqc/) (02_02). If adapters are still present, then the pipeline stops. |
| 02 | Indexing | Create genome or transcriptome index with [gmap_index](https://github.com/juliangehring/GMAP-GSNAP/blob/master/README), [bwa]() or [STAR]() |
| 03 | Mapping with GSNAP, BWA or STAR | Map trimmed reads (from step 01) on indexed reference genome or transcriptome. Convert outputs bam file containing only unique and concordant mapped reads |
| 04 | Process bam files | Optional process of bam files, includes sorting, filtering, cleaning... |
| 05 | Counting reads | Count how many aligned sequencing reads map to each feature in reference genome/transcriptome with [htseq-count](https://htseq.readthedocs.io/en/release_0.11.1/count.html) v0.9.1 |
| 06 | Differential expression analysis | Normalize count table (from step 05) and perform differential expression analysis using [Sartools](https://github.com/PF2-pasteur-fr/SARTools) R package v1.6.0 with R v3.3.2 |
| 07 | SNP calling with freebayes, mpileup or vcftools | Perform SNP calling on post processed bam files from step 04 |
| 99 | Statistics collection | Create trimming and mapping QC reports with [MultiQC](https://multiqc.info/) v1.5, merge them and write a QC report |

### Configuration
The configuration is done in the `pipeline.sh`, `config.yml` and `cluster.yml` files.

### Usage
Run pipeline 
```sh
$ sh pipeline.sh
```

Create dag of jobs
```sh
$  sh dag.sh
```

### References
- [![Snakemake](https://img.shields.io/badge/snakemake-≥3.5.2-brightgreen.svg?style=flat-square)](https://snakemake.bitbucket.io) Köster, Johannes and Rahmann, Sven. “Snakemake - A scalable bioinformatics workflow engine”. Bioinformatics 2012.
- Andrews S. (2010). FastQC: a quality control tool for high throughput sequence data. http://www.bioinformatics.babraham.ac.uk/projects/fastqc
-Bolger, A. M., Lohse, M., & Usadel, B. (2014). Trimmomatic: A flexible trimmer for Illumina Sequence Data. Bioinformatics, btu170.
- Thomas D. Wu and Colin K. Watanabe. GMAP: a genomic mapping and alignment program for mRNA and EST sequences. Bioinformatics 2005 21:1859-1875
-Thomas D. Wu and Serban Nacu. Fast and SNP-tolerant detection of complex variants and splicing in short reads. Bioinformatics 2010 26:873-881
- Li H.*, Handsaker B.*, Wysoker A., Fennell T., Ruan J., Homer N., Marth G., Abecasis G., Durbin R. and 1000 Genome Project Data Processing Subgroup (2009) The Sequence alignment/map (SAM) format and SAMtools. Bioinformatics, 25, 2078-9
- S Anders, T P Pyl, W Huber: HTSeq — A Python framework to work with high-throughput sequencing data. bioRxiv 2014. doi: 10.1101/002824
- H. Varet, L. Brillet-Guéguen, J.-Y. Coppee and M.-A. Dillies, SARTools: A DESeq2- and EdgeR-Based R Pipeline for Comprehensive Differential Analysis of RNA-Seq Data, PLoS One, 2016, doi: http://dx.doi.org/10.1371/journal.pone.0157022
- Philip Ewels, Måns Magnusson, Sverker Lundin and Max Käller. MultiQC: Summarize analysis results for multiple tools and samples in a single report. Bioinformatics (2016)



