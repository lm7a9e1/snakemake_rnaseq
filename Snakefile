workdir: config["workdir_path"]
report : "workflow.rst"

DATAPATH = config["rawdata_path"]

import pandas as pd
SAMPLE_NAMES = pd.read_csv(config["sample_info"])["SampleName"]

wildcard_constraints:
    mapping="gsnap|bwa|star",
    process=".*"

include: "rules/trimming.smk"
include: "rules/reference_index.smk"
include: "rules/mapping.smk"
include: "rules/bam_processing.smk"
include: "rules/de_analysis.smk"
include: "rules/snp_calling.smk"
include: "rules/tag_counting.smk"
include: "rules/general_stats.smk"

rule all:
    input:
        #"07_snp_calling/snpcalling_freebayes_refR3_gsnap_clean_f2_q5_F260_sorted_fixmate_markdup_sorted.vcf"
        #"07_snp_calling/snpcalling_freebayes_refR1_gsnap_clean_f2_q5_F260_fixmate_markdup_sorted_filtered.vcf", #works
        #"07_snp_calling/snpcalling_freebayes_refR2_bwa_f2_q5_F260_fixmate_markdup_sorted_filtered.vcf"
        #expand("04_bam_processing/{sample}_refR2_bwa_f2_q5_F260_fixmate_markdup_sorted.bam", sample=SAMPLE_NAMES), #_f2_q5_F260_sorted_fixmate_markdup.bam", sample=SAMPLE_NAMES)
        #"02_reference_index/star/R1"
        #expand("99_general_stats/fastqc_raw/{sample}_R1_fastqc.html", sample=SAMPLE_NAMES)#_f2_q5_F260_sorted_fixmate_markdup.bam", sample=SAMPLE_NAMES)
        #"02_reference_index/star/R1" #OK
        #expand("03_mapping/bam/{sample}_refR1_star.bam", sample=SAMPLE_NAMES) #OK
        #"06_de_analysis/sartools_refR1_star_f2_q5_F260_sorted"
        "07_snp_calling/snpcalling_mpileup_refgenome_star_f2_q5_F260_fixmate_markdup_sorted_filtered.vcf",
        "99_general_stats/fastqc_raw_multiqc_report.html",
        "99_general_stats/fastqc_trimmed_multiqc_report.html",
        "99_general_stats/flagstat#refgenome_gsnap_multiqc_report.html",
        "99_general_stats/flagstat#refgenome_gsnap_f2_q5_F260_multiqc_report.html",
        "99_general_stats/flagstat#refgenome_gsnap_f2_q5_F260_fixmate_multiqc_report.html",
        "99_general_stats/flagstat#refgenome_gsnap_f2_q5_F260_fixmate_markdup_multiqc_report.html",
        "99_general_stats/tag_counting#refgenome_gsnap_sorted_multiqc_report.html",
        "99_general_stats/tag_counting#refgenome_gsnap_f2_q5_F260_fixmate_markdup_sorted_multiqc_report.html"
